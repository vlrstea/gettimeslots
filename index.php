<?php

$array = [[0, 1], [3, 5], [4, 8], [10, 12], [4, 7], [9, 10]];
$arrai = [[0, 1], [3, 5], [4, 7], [4, 8], [9, 10], [10, 12]];//sorted array


function getTimeSlots(array $meetings){

	//sort the array ASC if there is more than a meeting
	if(count($meetings) > 1){
		//compare values in the sub-array and reorder based on the value
		usort($meetings, function($a,$b){
			return ($a[0] < $b[0]) ? -1 : 1;
		});
	}

	$timeslots = [];
	$filled = $meetings[0];
	for($i = 1; $i < count($meetings); $i++){

		$current = $meetings[$i];
		//if current meeting starts after the previous one
		if($current[0] > $filled[1] && $current[1] > $filled[1]){
			
			$timeslots[] = [$filled[1], $current[0]];
		}

		$filled[1] = max($current[1], $filled[1]);
	}

	return $timeslots;
}


var_dump(getTimeSlots($array));